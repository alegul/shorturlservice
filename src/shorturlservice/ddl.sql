CREATE SEQUENCE url_id_sequence START 2422350;

CREATE TABLE url
(
  id bigint DEFAULT nextval('url_id_sequence'),
  bigurl varchar
);