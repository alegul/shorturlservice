package main

import (
	"bytes"
	"fmt"
	"math"
	"strings"
)

var (
 alphabet= []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
 alphabetSize= len(alphabet)
)

// Функция преобразования целого числа в десятичном представлении в строковое представление
// в соответствии с заданным алфавитом
func Numtocode(number int64) (string) {
	// Первым шагом ищем максимальное число, которое является степенью основания системы счисления и при этом
	// меньше чем преобразуемое число, и определяем для него:
	// * position - степень
	// * positionFactor - множитель
	position := 0;
	var positionFactor int64
	for {
		divisionResult := number / int64(math.Pow(float64(alphabetSize), float64(position)));
		if divisionResult > 0 {
			positionFactor = divisionResult
			position++
		} else {
			position--
			break
		}
	}


	// Вторым шагом цикл:
	// * для каждой степени пишем в буфер соответствующий множителю символ
	// * вычисляем остаток, уменьшаем степень, вычисляем для него множитель
	// * повторяем
	var buffer bytes.Buffer
	for {
		buffer.WriteRune(alphabet[positionFactor])
		number = number - (positionFactor * int64(math.Pow(float64(alphabetSize), float64(position))))
		position--
		if position < 0 {
			break
		}

		positionFactor = number / int64(math.Pow(float64(alphabetSize), float64(position)));
	}

	return buffer.String()
}

// Функция преобразования числа, представленного как строка в заданном алфавите в целое число
// в десятичном представлении
func Codetonum(code string) (int64, error) {
	result := float64(0);
	add := float64(0);
	for charPos, char := range code {
		degree := len(code) - charPos - 1

		// todo возможно поиск индекса можно проще сделать, без конвертации в строки
		charIdx := strings.Index(string(alphabet), string(char));
		// Если в строке недопустимый символ, надо вернуть ошибку
		if (charIdx == -1) {
			return -1, fmt.Errorf("The character \"%s\" is not in the alphabet", string(char));
		}
		// Вычисляем десятичное представление для символа в текущей позиции
		add = float64(charIdx) * math.Pow(float64(alphabetSize), float64(degree));

		// todo исследовать, как правильно проверять переполнение целого числа при умножении
		// Пока проверка "на коленке" - было замечено что при перемножении больших чисел, когда результат
		// выходит за правую границу int64, получаются отрицательные значения. Такие строки получены
		// не из нашего сервиса и должны быть отброшены
		if (add  < 0 ) {
			return -1, fmt.Errorf("To big code %s", code);
		}
		result += add;
	}
	return int64(result), nil;
}
