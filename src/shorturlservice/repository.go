package main

import (
	"database/sql"
)

const (
	INSERT = "INSERT INTO Url (bigurl) VALUES ($1) RETURNING id"
	SELECT = "SELECT bigurl FROM Url WHERE id = $1 ";
)

// Структура для хранения параметров подключения к БД, а после инициализации - самого подключения к БД
// и стейтментов для взаимодействия с БД
type Repository struct {
	driverName string
	connectString string

	database *sql.DB
	SelectStmt *sql.Stmt
	InsertStmt *sql.Stmt
}

// Метод открытия БД
func (storage *Repository) openDatabase() (error) {
	database, err := sql.Open(storage.driverName, storage.connectString);
	if (err == nil) {
		// todo Изучить вопрос настройки пула потоков
		database.SetMaxOpenConns(10)
		storage.database = database
	}
	return err
}

// Метод подготовки стейтментов
func (storage *Repository) prepareStatements() (error) {
	// todo наверное можно выполнить инициализацию стейтментов более компактно, подумать
	InsertStmt, err := storage.database.Prepare(INSERT);
	if err != nil {
		return err
	}
	storage.InsertStmt = InsertStmt;

	SelectStmt, err := storage.database.Prepare(SELECT);
	if err != nil {
		return err
	}
	storage.SelectStmt = SelectStmt;
	return nil;
}

func (storage *Repository) closeDatabase() (error) {
	// todo возможно нужно prepared statements закрывать отдельно, проверить
	return storage.database.Close();
}

// Метод добавления в базу новой ссылки, возвращается id добавленной записи, или -1 если возникла ошибка
func (storage *Repository) saveLongUrl(longUrl string) (int64, error) {
	var id int64
	err := storage.InsertStmt.QueryRow(longUrl).Scan(&id)
	if err != nil {
		return -1, err
	}
	return id, nil;
}

// Метод получения ссылки по указанному id. Если при получении возникла ошибка или запись не найдена, возвращается
// пустая строка
func (storage *Repository)  GetLongUrl(number int64) (string, error) {
	var result string
	err := storage.SelectStmt.QueryRow(number).Scan(&result)
	if err != nil {
		return "", err
	}

	return result, nil;
}