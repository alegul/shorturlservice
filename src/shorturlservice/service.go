package main

import (
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/lib/pq"
	"log"
	"net/http"
)


// Настройки
// todo вынести настройки в отдельный конфигурационный файл
const dbDriverName = "postgres";
const dbConnectionString = "host=localhost dbname=urldb user=urlservice password=Qwe123 sslmode=disable";
const urlStart = "https://shorturl.ru/"
const port = "8080";

var controller *Controller

func init() {
	repository := Repository{driverName: dbDriverName, connectString: dbConnectionString}
	if err := repository.openDatabase(); err != nil {
		panic(err)
	}
	if err := repository.prepareStatements(); err != nil {
		panic(err)
	}
	controller = &Controller{repository: &repository};
}

func main(){
	defer func() {
		if err := controller.repository.closeDatabase(); err != nil {
			fmt.Println(err)
		}
	}()

	router := mux.NewRouter()
	router.HandleFunc("/short", controller.Short).Methods("POST")
	router.HandleFunc("/long", controller.Long).Methods("POST")

	log.Fatal(http.ListenAndServe(":" + port, router))
}




