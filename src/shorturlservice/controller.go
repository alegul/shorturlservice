package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/joomcode/errorx"
	"io/ioutil"
	"net/http"
	"strings"
)

// Типы ошибок, чтобы в HTTP-ответах указывать корректные коды
var (
	ControllerErrors        = errorx.NewNamespace("controller")
	InvalidDataErrorType = ControllerErrors.NewType("invalid_data")
	NotFoundErrorType = ControllerErrors.NewType("not_found")
)

// Структура, в которую преобразуются входящие и исходящие json
type UrlDesc struct {
	Url string `json:"url"`
}

// Контроллер, для обработки запросов
type Controller struct {
	repository *Repository
}

// Метод обработки запроса на получение короткой ссылки по длинной
func (controller *Controller) Short(w http.ResponseWriter, r *http.Request) {
	controller.processUrlRequest(w, r,
		func (longUrl string) (string, error) {
			if len(longUrl) == 0 {
				return "", InvalidDataErrorType.New("Url is empty")
			}

			if !(strings.HasPrefix(longUrl, "http://") || strings.HasPrefix(longUrl, "https://") ) {
				return "", InvalidDataErrorType.New("Url %s has incorrect protocol", longUrl);
			}
			// todo проверку что после протокола идёт hostname или IP

			// Сохранение длинного url в БД, возвращается id записи
			id, err := controller.repository.saveLongUrl(longUrl);
			if err != nil {
				return "", err
			}
			return fmt.Sprintf("%s%s", urlStart , Numtocode(id)), nil;
		})
}

// Метод обработки запроса на получение длинной ссылки по короткой
func (controller *Controller) Long(w http.ResponseWriter, r *http.Request) {
	controller.processUrlRequest(w, r,
		func (shortUrl string) (string, error) {
			// Тримим слева и справа на всякий случай
			shortUrl = strings.TrimSpace(shortUrl)

			// Выполняем проверки короткого адреса
			if len(shortUrl) == 0 {
				return "", InvalidDataErrorType.New("Url is empty")
			}

			if !strings.HasPrefix(shortUrl, urlStart) {
				return "", InvalidDataErrorType.New(
					"Incorrect Url %s - prefix is not %s",
					shortUrl, urlStart)
			}

			// Извлекаем код и проверяем что он не нулевой длины
			code := shortUrl[len(urlStart):]
			if len(code) == 0 {
				return "", InvalidDataErrorType.New("Code is empty")
			}

			// Трансформируем код в 10е число
			id, err := Codetonum(code);
			if err != nil {
				return "", InvalidDataErrorType.Wrap(err, "Cant transform code to num");
			}

			// Получаем длинный url из базы
			longUrl, err := controller.repository.GetLongUrl(id);
			if err != nil {
				// Особым образом обрабатываем ошибку ErrNoRows, чтобы при возникновении вернуть клиенту 404 статус
				if (err == sql.ErrNoRows) {
					return "", NotFoundErrorType.Wrap(err,
						"Cant find long url for short url %s, id %d",
						shortUrl, id);
				}
				return "", err
			}
			return longUrl, nil;
		})

}

// Общий метод обработки запроса, реализующий чтение сходящего запроса, разбор json, формирование ответного json
func (controller *Controller) processUrlRequest(w http.ResponseWriter, r *http.Request, f func(string) (string, error)){
	defer func() {
		if err := r.Body.Close(); err !=nil {
			fmt.Println(err)
		}
	}()

	// Получение тела запроса
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		fmt.Println(err)
		_, _ = w.Write([]byte(fmt.Sprintf("Body read error: %q", err)))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	// Разбор тела запроса в json
	var inUrlDesc UrlDesc
	err = json.Unmarshal(bodyBytes, &inUrlDesc );
	if err != nil {
		fmt.Println(err)
		_, _ = w.Write([]byte(fmt.Sprintf("Invalid body [%s], unmarshal error: %q", string(bodyBytes), err)))
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	// Трансформация url из входящего json с помощью функции
	outUrl, err := f(inUrlDesc.Url);
	if err != nil {
		// Анализ типа ошибки, определение подходящего http-кода
		var header int;
		if errorx.IsOfType(err, NotFoundErrorType) {
			header = http.StatusNotFound
		} else if errorx.IsOfType(err, InvalidDataErrorType){
			header = http.StatusBadRequest
		} else {
			header = http.StatusInternalServerError
		}
		w.WriteHeader(header);
		_, _ = w.Write([]byte(err.Error()));
		return;
	}


	if bodyBytes, err = json.Marshal(&UrlDesc{Url: outUrl}); err == nil {
		w.Header().Set("Content-Type", "application/json")
		_, _ = w.Write(bodyBytes)
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(fmt.Sprintf("Cant marshall json, error: %q", string(bodyBytes), err)))

	}
}
